from api.models import Section, SubSection, Question, Answer
from django.test import TestCase
from django.urls import reverse
from faker import Faker
import json


def create_models():
    faker = Faker()
    for i in range(10):
        section = Section.objects.create(
            name=faker.name()
        )
        sub_section = SubSection.objects.create(
            section=section,
            label=faker.job(),
            pretty_label=faker.email(),
            description="""
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
            do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
            Ut enim ad minim veniam, quis nostrud exercitation ullamco 
            laboris nisi ut aliquip ex ea commodo consequat. Duis aute 
            irure dolor in reprehenderit in voluptate velit esse cillum 
            dolore eu fugiat nulla pariatur. Excepteur sint occaecat 
            cupidatat non proident, sunt in culpa qui officia deserunt 
            mollit anim id est laborum.
            """
        )
        for j in range(10):
            sub_section.questions.create(
                name="Question {}".format(j),
                description="""
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
            do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            """,
                content="""
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
            do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
            Ut enim ad minim veniam, quis nostrud exercitation ullamco 
            laboris nisi ut aliquip ex ea commodo consequat. Duis aute 
            irure dolor in reprehenderit in voluptate velit esse cillum 
            dolore eu fugiat nulla pariatur. Excepteur sint occaecat 
            cupidatat non proident, sunt in culpa qui officia deserunt 
            mollit anim id est laborum.
            """
            )


class SectionGraphGLTestCase(TestCase):

    def setUp(self):
        create_models()
        self.url = '/section/'

    def test_section_graph_response(self):
        query = '''
            {
                allSections {
                    id
                    name
                    section {
                        label,
                        prettyLabel,
                        description
                    }
                }
            }
        '''
        response = self.client.post(self.url, data={'query': query})
        response_length = len(response.json().get(
            'data', {}).get(
            'allSections', [])
        )
        self.assertTrue(response_length == 10,
                        msg="Expected a length of 10 got "
                            "{}".format(response_length))
        subsection_length = len(response.json().get(
            'data', {}).get(
            'allSections', [])[0].get(
            'section')
        )
        self.assertTrue(subsection_length == 1,
                        msg="Expected first section sub section to have 1 "
                            "value got {}".format(subsection_length))


class SubSectionGraphQLTestCase(TestCase):

    def setUp(self):
        create_models()
        # This just fills the sections and subsections with random values
        self.url = '/sub/section/'
        query = '''
            {
                subSection(id:%s){
                    prettyLabel,
                        description,
                        questions{
                            name,
                            content,
                            answerSet{
                            value,
                            label
                        }
                    }
                }
            }
        ''' % SubSection.objects.first().id
        self.response = self.client.post(self.url, data={'query': query})

    def test_subsection_graph_response(self):
        payload = self.response.json().get('data', {}).get('subSection', None)
        self.assertTrue(payload is not None, msg="Expected Payload got None")

    def test_sub_section_graph_has_questions(self):
        questions_length = len(self.response.json().get(
            'data', {}).get(
            'subSection', {}).get(
            'questions', [])
        )
        self.assertTrue(questions_length == 10,
                        msg="Expected 10 questions git "
                            "{}".format(questions_length))


class CreateQuestionsApiTestCase(TestCase):
    def setUp(self):
        create_models()
        self.sub_section = SubSection.objects.first()
        self.url = reverse('questions-list')

    def test_create_question(self):
        data = {
            "section": [self.sub_section.id],
            "name": "Test Create Question",
            "description": "Creating a test question",
            "content": "This is a test Question"
        }
        resp = self.client.post(self.url, data=data)
        self.assertTrue(resp.status_code == 201,
                        msg="Invalid Create Status Code, expected 201 got "
                            "{}".format(resp.status_code))


class SubSectionApiTestCase(TestCase):
    def setUp(self):
        create_models()
        self.section = Section.objects.first()
        self.url = reverse('sub-sections-list')

    def test_create_sub_section(self):
        data = {
            "section": [self.section.id],
            "label": "Test Create Sub Section",
            "pretty_label": "Creating a test sub section",
            "description": "This is a test Question"
        }
        resp = self.client.post(self.url, data=data)
        self.assertTrue(resp.status_code == 201,
                        msg="Invalid Create Status Code, expected 201 got "
                            "{}".format(resp.status_code))

    def test_update_sub_section(self):
        sub_section = SubSection.objects.first()
        data = {
            "label": "Changed Sub Section",
        }
        resp = self.client.patch("{}{}/".format(self.url, sub_section.id),
                                 data=json.dumps(data),
                                 content_type="application/json")
        self.assertTrue(resp.status_code == 200,
                        msg="Invalid Update Status Code, expected 200 got "
                            "{}".format(resp.status_code))
        sub_section = SubSection.objects.get(id=sub_section.id)
        self.assertTrue(sub_section.label == "Changed Sub Section",
                        msg="Patch Did Not Update Object")

    def test_delete_sub_section(self):
        sub_section = SubSection.objects.first()
        resp = self.client.delete("{}{}/".format(self.url, sub_section.id))
        self.assertTrue(resp.status_code == 204,
                        msg="Invalid Update Status Code, expected 204 got "
                            "{}".format(resp.status_code))


class AnswerApiTestCase(TestCase):
    def setUp(self):
        create_models()
        self.question = Question.objects.first()
        self.url = reverse('answer-list')

    def test_create_answer(self):
        data = {
            "question": self.question.id,
            "label": "Test Create Sub Section",
            "value": "false",
        }
        resp = self.client.post(self.url, data=data)
        self.assertTrue(resp.status_code == 201,
                        msg="Invalid Create Status Code, expected 201 got "
                            "{}".format(resp.status_code))

    def test_update_answer(self):
        answer = Answer.objects.create(**{
            "question": self.question,
            "label": "Test Create Sub Section",
            "value": "false",
        })
        data = {
            "label": "Changed Answer",
        }
        resp = self.client.patch("{}{}/".format(self.url, answer.id),
                                 data=json.dumps(data),
                                 content_type="application/json")
        self.assertTrue(resp.status_code == 200,
                        msg="Invalid Update Status Code, expected 200 got "
                            "{}".format(resp.status_code))
        answer = Answer.objects.get(id=answer.id)
        self.assertTrue(answer.label == "Changed Answer",
                        msg="Patch Did Not Update Object")

    def test_delete_answer(self):
        answer = Answer.objects.create(**{
            "question": self.question,
            "label": "Test Create Sub Section",
            "value": "false",
        })
        resp = self.client.delete("{}{}/".format(self.url, answer.id))
        self.assertTrue(resp.status_code == 204,
                        msg="Invalid Update Status Code, expected 204 got "
                            "{}".format(resp.status_code))
