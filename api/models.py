from django.db import models


class Section(models.Model):
    name = models.CharField(max_length=256, unique=True)

    def __str__(self):
        return self.name


class SubSection(models.Model):
    section = models.ForeignKey(Section, on_delete=models.CASCADE,
                                related_name='section')
    label = models.CharField(max_length=100)
    pretty_label = models.CharField(max_length=100)
    description = models.TextField(max_length=2000)

    def __str__(self):
        return self.label


class Question(models.Model):
    section = models.ManyToManyField(SubSection, related_name='questions')
    name = models.CharField(max_length=2000)
    description = models.TextField(max_length=2000)
    content = models.TextField(max_length=2000)

    def __str__(self):
        return self.name


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    label = models.CharField(max_length=256)
    value = models.CharField(max_length=256)

    def __str__(self):
        return self.label


class Dashboard(models.Model):
    name = models.CharField(max_length=256)
    action_plan = models.CharField(max_length=512)
    content_alignment = models.CharField(max_length=64)
    weightage = models.IntegerField()
    text_label = models.CharField(max_length=128)
    text_input = models.BooleanField(default=False)
    use_dashboard = models.BooleanField(default=False)
    use_remarks = models.BooleanField(default=False)

    def __str__(self):
        return self.name


