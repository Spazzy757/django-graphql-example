from api.models import Question, SubSection, Answer
from rest_framework.viewsets import ModelViewSet
from api.serializers import (
    QuestionSerializer,
    SubSectionSerializer,
    AnswerSerializer
)


# Create your views here.
class QuestionViewSet(ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class SubSectionViewSet(ModelViewSet):
    queryset = SubSection.objects.all()
    serializer_class = SubSectionSerializer


class AnswerViewSet(ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
