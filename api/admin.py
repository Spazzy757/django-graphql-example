from django.contrib import admin
from api.models import Section, SubSection, Question, Answer

# Register your models here.
admin.site.register(Section)
admin.site.register(Question)
admin.site.register(SubSection)
admin.site.register(Answer)
