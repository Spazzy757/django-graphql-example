from api.models import Section, SubSection, Question, Answer
from graphene_django import DjangoObjectType
import graphene


class SectionType(DjangoObjectType):
    class Meta:
        model = Section


class SubSectionType(DjangoObjectType):
    class Meta:
        model = SubSection


class QuestionType(DjangoObjectType):
    class Meta:
        model = Question


class AnswerType(DjangoObjectType):
    class Meta:
        model = Answer


class SectionQueryFormat(object):
    all_sections = graphene.List(SectionType)
    all_sub_sections = graphene.List(SubSectionType)
    section = graphene.Field(SectionType, id=graphene.Int())
    sub_section = graphene.Field(SubSectionType, id=graphene.Int())

    @staticmethod
    def resolve_section(*args, **kwargs):
        object_id = kwargs.get('id')
        if id:
            return Section.objects.get(pk=object_id)
        return None

    @staticmethod
    def resolve_sub_section(*args, **kwargs):
        object_id = kwargs.get('id')
        if id:
            return SubSection.objects.get(pk=object_id)
        return None

    @staticmethod
    def resolve_all_sections(*args, **kwargs):
        return Section.objects.all()

    @staticmethod
    def resolve_all_sub_sections(*args, **kwargs):
        return SubSection.objects.select_related('section').all()


class SubSectionQueryFormat(object):
    all_questions = graphene.List(QuestionType)
    sub_section = graphene.Field(SubSectionType, id=graphene.Int())

    @staticmethod
    def resolve_sub_section(*args, **kwargs):
        object_id = kwargs.get('id')
        if id:
            return SubSection.objects.get(pk=object_id)
        return None

    @staticmethod
    def resolve_all_questions(*args, **kwargs):
        return Question.objects.select_related('sub_sections').all()


class QuestionQueryFormat(object):
    all_answers = graphene.List(AnswerType)
    question = graphene.Field(Question, id=graphene.Int())

    @staticmethod
    def resolve_question(*args, **kwargs):
        object_id = kwargs.get('id')
        if id:
            return Question.objects.get(pk=object_id)
        return None

    @staticmethod
    def resolve_all_answers(*args, **kwargs):
        return Answer.objects.select_related('question').all()


class SectionQuery(SectionQueryFormat, graphene.ObjectType):
    pass


class SubSectionQuery(SubSectionQueryFormat, graphene.ObjectType):
    pass


section_schema = graphene.Schema(query=SectionQuery)
sub_section_schema = graphene.Schema(query=SubSectionQuery)
