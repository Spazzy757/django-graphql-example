from api.views import QuestionViewSet, SubSectionViewSet, AnswerViewSet
from rest_framework.routers import DefaultRouter
from django.urls import re_path, include

router = DefaultRouter()

router.register(
    prefix=r'questions',
    viewset=QuestionViewSet,
    base_name='questions'
)

router.register(
    prefix=r'sub/sections',
    viewset=SubSectionViewSet,
    base_name='sub-sections'
)

router.register(
    prefix=r'answer',
    viewset=AnswerViewSet,
    base_name='answer'
)

urlpatterns = [
    re_path(r'^', include(router.urls)),
]
