from rest_framework.serializers import ModelSerializer
from api.models import Question, SubSection, Answer


class QuestionSerializer(ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'


class SubSectionSerializer(ModelSerializer):
    class Meta:
        model = SubSection
        fields = '__all__'


class AnswerSerializer(ModelSerializer):
    class Meta:
        model = Answer
        fields = '__all__'
