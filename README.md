# A Test Project Using Django, GraphQL and Vue.js

To Setup for the first time
```bash
docker-compose build
docker-compose run --rm web python manage.py migrate
docker-compose run --rm web python manage.py loaddata data.json
docker-compose up
```
> Migrations might fail the first time, just rerun them again once your database has started atleast once

Visit http://localhost:8000/admin for djangos admin interface and http://localhost:8001 for the frontend

```
username: admin
password: test1234
```

To Load Dummy Data
```bash
docker-compose run --rm web python manage.py loaddata data.json
```

To Start (requires docker and docker-compose)
```bash
docker-compose up
# In detached Mode
docker-compose up -d
```

To Run Test for django app
```bash
docker-compose run --rm coverage manage.py test
# To Check Test Coverage
docker-compose run --rm web coverage report
```

To Rebuild UI
```bash
docker-compose build frontend
```
