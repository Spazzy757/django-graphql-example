import os
DB_PASSWORD = os.environ.get('APPOINTMENTSERVICE_DATABASE_PASSWORD')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('DATABASE_NAME', 'postgres'),
        'USER': os.environ.get('DATABASE_USER', 'postgres'),
        'HOST': os.environ.get('DATABASE_HOST', 'db'),
        'PORT': os.environ.get('POSTGRES_DB_PORT', '5432'),
        'PASSWORD': DB_PASSWORD,
    }
}

GRAPHENE = {
    'SCHEMA': 'api.schema.schema'  # Where your Graphene schema lives
}

CORS_ORIGIN_ALLOW_ALL=True
